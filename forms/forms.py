from django import forms
from django.core.exceptions import ValidationError
from .models import *

class CommentForm(forms.ModelForm):
	class Meta:
		model = Comments
		fields = '__all__'

	def __init__(self, *args, **kwargs):
		super(CommentForm, self).__init__(*args, **kwargs)

		self.fields['email'].widget.attrs.update({'class': 'form-control cyan-back'})
		self.fields['comment'].widget.attrs.update({'class': 'form-control cyan-back'})

	def clean_email(self):
		if (self.cleaned_data.get('email', '').endswith('not.ok')):
			raise ValidationError('Invalid email address.')

		return self.cleaned_data.get('email', '')

	def clean(self):
		self.cleaned_data = super(CommentForm, self).clean()
		if ('die' in self.cleaned_data.get('comment', '')):
			raise ValidationError('please enter a good comment.')

		if (self.cleaned_data.get('email', '').endswith('hotmail.com')):
			raise ValidationError('we hate hotmail!')
		
		return self.cleaned_data