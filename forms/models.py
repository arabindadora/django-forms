from django.db import models

class Comments(models.Model):
	email = models.EmailField(max_length=50)
	comment = models.CharField(max_length=200)