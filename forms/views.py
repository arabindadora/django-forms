from django.shortcuts import render, redirect
from .models import *
from .forms import *

def index(request):
	all_comments = Comments.objects.all()
	return render(request, 'home.html', {'all_comments': all_comments})

def comment(request):
	if request.method == 'POST':
		form = CommentForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/')
	else:
		form = CommentForm()
	return render(request, 'comment.html', {'form': form })

