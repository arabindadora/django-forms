# Django model forms sample app #

### What is this repository for? ###

* This app implements Django model forms
* Version: 0.0.1
* Documentation: https://drivedev.atlassian.net/wiki/display/BD/django+model+forms+usage+and+caveats

### How do I get set up? ###

* git clone this repo
* Navigate to the directory
* Run migrations:  `python manage.py makemigrations forms && python manage.py migrate`
* Start the app: `python manage.py runserver`
* View in browser: `localhost:8000`

### Who do I talk to? ###

* arabinda.dora@ggktech.com